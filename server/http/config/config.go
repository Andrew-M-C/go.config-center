package config

import (
	httpsess "gitlab.com/Andrew-M-C/go.httpsess"
	mysqlkv "gitlab.com/Andrew-M-C/go.mysqlkv"
)

// Endpoint 表示一个示例接口实现
type Endpoint interface {
	QueryConfig(sess httpsess.Context)
	UpdateConfig(sess httpsess.Context)
}

// NewEndpoint 返回一个实现
func NewEndpoint(kv mysqlkv.Storage) (Endpoint, error) {
	impl := &endpointImpl{
		kv: kv,
	}
	return impl, nil
}
