package protocol

// QueryConfigRequest 表示 queryConfig 请求
type QueryConfigRequest struct {
	Domain string `json:"domain"`
	// Depth  int    `json:"depth,omitempty"`
}

// QueryConfigResponseData 表示 queryConfig 响应 data 部分。
// 这个建议是被调方自行构建 struct 或者是自行解析
type QueryConfigResponseData map[string]interface{}

// UpdateConfigRequest 表示 updateConfig 请求
type UpdateConfigRequest map[string]interface{}
