package config

import (
	"errors"
	"fmt"
	"strings"

	jsonvalue "github.com/Andrew-M-C/go.jsonvalue"
	"gitlab.com/Andrew-M-C/go.config-center/server/http/config/protocol"

	httpsess "gitlab.com/Andrew-M-C/go.httpsess"
	mysqlkv "gitlab.com/Andrew-M-C/go.mysqlkv"
)

type endpointImpl struct {
	kv mysqlkv.Storage
}

// QueryConfig 实现接口
func (impl *endpointImpl) QueryConfig(sess httpsess.Context) {
	l := sess.Logger()
	defer sess.PackResponse()

	l.Infof(">>>> QueryConfig")
	defer l.Infof("<<<< QueryConfig")

	req := queryConfigSessReq{}
	err := req.unmarshalAndValidagteQueryConfigReq(sess, impl)
	if err != nil {
		return
	}

	// 往DB中查询数据
	err = req.queryKVs()
	if err != nil {
		return
	}

	return
}

type queryConfigSessReq struct {
	protocol.QueryConfigRequest

	sess httpsess.Context
	impl *endpointImpl

	kvObj string
	kvKey string
	ctx   httpsess.Context
}

func (req *queryConfigSessReq) unmarshalAndValidagteQueryConfigReq(
	sess httpsess.Context, impl *endpointImpl,
) (err error) {
	l := sess.Logger()

	err = sess.Unmarshal(&req)
	if err != nil {
		l.Error(err)
		req.ctx.SetError(httpsess.ParamError, err)
		return
	}

	if req.Domain == "" {
		req.ctx.SetError(httpsess.ParamError, errors.New("missing domain"))
		return
	}

	domains := strings.Split(req.Domain, ".")
	req.kvObj = domains[0]
	req.kvKey = strings.Join(domains[1:], ".")

	req.impl = impl
	req.sess = sess

	return nil
}

func (req *queryConfigSessReq) queryKVs() (err error) {
	impl := req.impl
	sess := req.sess
	l := sess.Logger()

	l.Debugf("req: %s, %s", req.kvObj, req.kvKey)
	var keys []string

	if req.kvKey != "" {
		keys = []string{req.kvKey}
	}
	kvs, err := impl.kv.GetToJSONValue(0, req.kvObj, keys)
	if err != nil {
		err = fmt.Errorf("query in DB error: %w", err)
		sess.SetError(httpsess.DatabaseError, err)
		return
	}

	l.Debugf("Got kvs: %v", kvs)
	// resp := jsonvalue.NewObject()
	// resp.Set(kvs).At(req.kvObj)

	lastKey := ""

	if req.kvKey == "" {
		sess.SetResponse(kvs)
		lastKey = req.kvObj
	} else {
		keys = strings.Split(req.kvKey, ".")
		lastKey = keys[len(keys)-1]
		intf := []interface{}{}
		for _, k := range keys {
			intf = append(intf, k)
		}
		kvs, _ = kvs.Get(keys[0], intf[1:]...)
	}

	resp := jsonvalue.NewObject()
	resp.Set(kvs).At(lastKey)
	sess.SetResponse(resp)
	return nil
}
