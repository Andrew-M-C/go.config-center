package config

import (
	"errors"
	"fmt"

	httpsess "gitlab.com/Andrew-M-C/go.httpsess"
)

// UpdateConfig 实现接口
func (impl *endpointImpl) UpdateConfig(sess httpsess.Context) {
	l := sess.Logger()
	defer sess.PackResponse()

	l.Infof(">>>> QueryConfig")
	defer l.Infof("<<<< QueryConfig")

	req, err := sess.UnmarshalToJsonvalue()
	if err != nil {
		err = fmt.Errorf("unmarshal request error: %w", err)
		sess.SetError(httpsess.ParamError, err)
		l.Error(err)
		return
	}

	if !req.IsObject() {
		err = errors.New("request is not an object")
		sess.SetError(httpsess.ParamError, err)
		l.Error(err)
		return
	}

	for it := range req.IterObjects() {
		err = impl.kv.SetByJSONValue(0, it.K, it.V)
		if err != nil {
			err = fmt.Errorf("set %s error: %w", it.K, err)
			sess.SetError(httpsess.DatabaseError, err)
			l.Error(err)
			return
		}
	}

	sess.SetResponse("{}")
	return
}
