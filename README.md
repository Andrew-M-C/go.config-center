# go.config-center

基于 MySQL 实现的配置中心

这个服务，我尝试使用 grpc 来做 RPC，但暂时还不成功，因为 etcd 和依赖了 gprc，版本有冲突

- 本地调试启动命令：

- `ETCD_ENDPOINTS=localhost:21379,localhost:22379,localhost:23379 HTTP_PORT=8002 go run .`

- docker 调试命令:

```shell
docker run -itd \
-e ETCD_ENDPOINTS=host.docker.internal:21379,host.docker.internal:22379,host.docker.internal:23379 \
-e LOG_FILE=1 \
-e HTTP_PORT=80 \
-p 8002:80 \
--restart always \
--name config-center \
config-center:3cac3f6
```

## 参考资料

- [gRPC-GO 筆記]