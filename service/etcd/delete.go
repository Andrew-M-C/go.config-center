package etcd

import "context"

// DeleteRequest 模拟 clientv3.etcdserverpbDeleteRangeRequest
type DeleteRequest struct {
	Key      string `json:"key"` // b64
	PrevKv   bool   `json:"prev_kv"`
	RangeEnd string `json:"range_end"` // b64
}

// DeleteResponse 模拟 clientv3.etcdserverpbDeleteRangeResponse
type DeleteResponse struct {
	Header  ResponseHeader `json:"header"`
	Deleted string         `json:"deleted"`
	PrevKvs []*Kv          `json:"prev_kvs"`
}

// Delete 模拟 kv/deleterange 请求
func (cli *Client) Delete(ctx context.Context, k string) (resp *DeleteResponse, err error) {
	req := DeleteRequest{Key: b64Enc(k)}
	rsp := DeleteResponse{}

	err = cli.post(ctx, "kv/deleterange", &req, &rsp)
	if err != nil {
		return
	}

	for _, kv := range resp.PrevKvs {
		kv.Key = b64Dec(kv.Key)
		kv.Value = b64DecB(kv.ValueInString)
	}
	return &rsp, nil
}
