package etcd

type opType int

const (
	// A default Op has opType 0, which is invalid.
	tRange opType = iota + 1
	tPut
	tDeleteRange
	tTxn
)

type Op struct {
	t   opType
	key []byte
	// end []byte

	// for put
	val []byte
	// leaseID LeaseID
}

// OpPut 模拟实现 clientv3 的 OpPut
func OpPut(key, val string) Op {
	ret := Op{t: tPut, key: []byte(key), val: []byte(val)}
	return ret
}

// OpOption configures Operations like Get, Put, Delete.
type OpOption struct {
	rangeEnd string
}

// WithRange 模拟实现 clientv3 的 WithRange
func WithRange(endKey string) *OpOption {
	return &OpOption{
		rangeEnd: endKey,
	}
}

// RequestOp 对应 clientv3.etcdserverpbRequestOp，省略了一些不需要的字段
type RequestOp struct {
	RequestPut *PutRequest `json:"request_put,omitempty"`
}
