package etcd

// PutRequest 对应 clientv3.etcdserverpbPutRequest
type PutRequest struct {
	Key   string `json:"key"`   // base64
	Value string `json:"value"` // base64
	Lease string `json:"lease,omitempty"`
}
