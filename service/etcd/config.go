package etcd

import (
	"encoding/base64"
	"time"
)

// Config 表示 etcd v3 client 相关配置
type Config struct {
	Endpoints   []string
	DialTimeout time.Duration
}

func b64Enc(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

func b64EncB(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func b64DecB(s string) []byte {
	b, _ := base64.StdEncoding.DecodeString(s)
	return b
}

func b64Dec(s string) string {
	b, _ := base64.StdEncoding.DecodeString(s)
	if len(b) == 0 {
		return ""
	}
	return string(b)
}
