package etcd

// ResponseHeader 对应 clientv3.etcdserverpbResponseHeader.properties
type ResponseHeader struct {
	ClusterID string `json:"cluster_id"`
	MemberID  string `json:"member_id"`
	RaftTerm  string `json:"raft_term"`
	Revision  string `json:"revision"`
}
