package etcd

import (
	"context"

	log "github.com/sirupsen/logrus"
)

type Txn interface {
	// If takes a list of comparison. If all comparisons passed in succeed,
	// the operations passed into Then() will be executed. Or the operations
	// passed into Else() will be executed.
	// If(cs ...Cmp) Txn

	// Then takes a list of operations. The Ops list will be executed, if the
	// comparisons passed in If() succeed.
	Then(ops ...Op) Txn

	// Else takes a list of operations. The Ops list will be executed, if the
	// comparisons passed in If() fail.
	// Else(ops ...Op) Txn

	// Commit tries to commit the transaction.
	Commit() (*TxnResponse, error)
}

// Txn 模拟 clientv3.Client.Txn
func (cli *Client) Txn(ctx context.Context) Txn {
	return &txn{
		ctx: ctx,
		cli: cli,
	}
}

// txn 模拟 clientv3.txn
type txn struct {
	ctx context.Context

	// cif   bool
	cthen bool
	celse bool

	// 以下是自定义的字段，与 clientv3 不通
	opPut []*Op
	cli   *Client
}

func (txn *txn) Then(ops ...Op) Txn {
	if txn.cthen {
		log.Fatalf("cannot call Then twice!")
	}
	if txn.celse {
		log.Fatalf("cannot call Then after Else!")
	}

	txn.cthen = true

	for i := range ops {
		op := &ops[i]
		switch op.t {
		default:
			log.Fatalf("invalid op type: %v", op.t)
		case tPut:
			txn.opPut = append(txn.opPut, op)
		}
	}

	return txn
}

func (txn *txn) Commit() (*TxnResponse, error) {
	resp := TxnResponse{}
	req := TxnRequest{}

	for _, op := range txn.opPut {
		r := RequestOp{}
		r.RequestPut = &PutRequest{
			Key:   b64EncB(op.key),
			Value: b64EncB(op.val),
		}
		req.Success = append(req.Success, &r)
	}

	err := txn.cli.post(txn.ctx, "kv/txn", &req, &resp)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

// TxnRequest 对应 clientv3.etcdserverpbTxnRequest
type TxnRequest struct {
	// Compare []*Compare `json:"compare,omitempty"`
	// Failure []*RequestOp `json:"failure,omitempty"`
	Success []*RequestOp `json:"success,omitempty"`
}

// TxnResponse 对应 clientv3.etcdserverpbTxnResponse
type TxnResponse struct {
	Header    ResponseHeader `json:"header"`
	Succeeded bool           `json:"succeeded"`
}
