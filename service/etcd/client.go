package etcd

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	jsonvalue "github.com/Andrew-M-C/go.jsonvalue"
	log "github.com/sirupsen/logrus"
)

// Client 表示 etcd v3 的一个 client 实现。目前只实现了 kv 所需的接口
type Client struct {
	endpoints []*etcdEp
	current   int

	timeout struct {
		dial      time.Duration
		keepalive time.Duration
	}

	epReq chan struct{}
	epRsp chan *etcdEp
}

// etcdEp
type etcdEp struct {
	host        string
	dialTimeout time.Duration
	lastAlive   time.Time
	err         error // 用于返回
}

func New(conf Config) (*Client, error) {
	cli := Client{
		current: rand.Intn(len(conf.Endpoints)),
		epReq:   make(chan struct{}, 256),
		epRsp:   make(chan *etcdEp, 256),
	}
	for _, h := range conf.Endpoints {
		cli.endpoints = append(cli.endpoints, &etcdEp{
			host:        h,
			dialTimeout: conf.DialTimeout,
			lastAlive:   time.Time{},
		})
	}

	cli.timeout.dial = conf.DialTimeout
	cli.timeout.keepalive = conf.DialTimeout / 4

	ch := make(chan error)
	go cli.doKeepAlive(ch)

	if err := <-ch; err != nil {
		err = fmt.Errorf("connect etcd endpoint error: %w", err)
		return nil, err
	}

	return &cli, nil
}

// doKeepAlive 与各个 endpoint 保持连接，并获取上一次可用的连接
func (cli *Client) doKeepAlive(connRes chan error) {
	ep := cli.endpoints[cli.current]
	a := base64.StdEncoding.EncodeToString([]byte("a"))

	iterDone := make(chan struct{})

	// initConnOK 表示是否至少成功连接过
	initConnOK := false
	// connTryCount 表示依次尝试连接几个 endpoint，只有尝试过了均失败才会返回错误
	connTryCount := 0
	// shouldExit 表示是不是应该退出 keepalive
	shouldExit := make(chan struct{})
	defer close(shouldExit)

	iterEndpoint := func() {
		defer func() {
			iterDone <- struct{}{}
		}()
		// 尝试连接
		httpcli := &http.Client{
			Transport: http.DefaultTransport,
			Timeout:   cli.timeout.dial,
		}
		u := fmt.Sprintf("http://%s/v3/kv/range", ep.host)
		j := jsonvalue.NewObject(map[string]interface{}{"key": a})

		keepalive := func() (err error) {
			defer func() {
				if err == nil {
					if !initConnOK {
						log.Infof("connected to %v", ep.host)
						initConnOK = true
						connRes <- nil
						close(connRes)
					}
					return
				}
				if !initConnOK {
					log.Infof("connected to %v error: %v", ep.host, err)
					if connTryCount == len(cli.endpoints)-1 {
						connRes <- err
						close(connRes)
						shouldExit <- struct{}{}
					}
					connTryCount++
					return
				}
			}()

			httpres, err := httpcli.Post(u, "application/json;charset=utf-8", bytes.NewBuffer(j.MustMarshal()))
			if err != nil {
				log.Infof("httpcli.Post error: %v", err)
				return err
			}
			defer httpres.Body.Close()

			if httpres.StatusCode != 200 {
				log.Infof("%s returns StatusCode %v", ep.host, httpres.Status)
				return errors.New(httpres.Status)
			}

			// OK
			// b, err := ioutil.ReadAll(httpres.Body)
			// if err != nil {
			// 	log.Infof("ioutil.ReadAll error: %v", err)
			// 	return err
			// }

			// log.Infof("request %v, got response: %v", ep.host, string(b))
			return nil
		}

		logged := false

		err := keepalive()
		for err == nil {
			if !logged {
				logged = true
				log.Infof("connect %s OK", ep.host)
			}
			ep.lastAlive = time.Now()
			time.Sleep(cli.timeout.keepalive)
			err = keepalive()
		}
	}

	for {
		go iterEndpoint()

		keepWaitIter := true
		for keepWaitIter {
			select {
			case <-iterDone:
				keepWaitIter = false

				cli.current++
				if cli.current >= len(cli.endpoints) {
					cli.current = 0
				}

				ep = cli.endpoints[cli.current]
				log.Infof("iterEndpoint done, should switch endpoint to [%02d]-%s", cli.current, ep.host)

			case <-cli.epReq:
				ep = cli.endpoints[cli.current]
				if time.Since(ep.lastAlive) > cli.timeout.dial {
					ep = &etcdEp{err: fmt.Errorf("endpoint disconnected, last alive %v", ep.lastAlive)}
				}
				cli.epRsp <- ep

			case <-shouldExit:
				log.Infof("exit keepAlive")
				return
			}
		}
	}
}

func (cli *Client) getEndpoint() (*etcdEp, error) {
	cli.epReq <- struct{}{}
	ep := <-cli.epRsp
	if ep.err != nil {
		return nil, ep.err
	}
	return ep, nil
}

func (cli *Client) post(ctx context.Context, function string, req, rsp interface{}) (err error) {
	ep, err := cli.getEndpoint()
	if err != nil {
		return fmt.Errorf("getEndpoint error: %w", err)
	}

	httpcli := &http.Client{Transport: http.DefaultTransport}

	u := fmt.Sprintf("http://%s/v3/%s", ep.host, function)

	b, _ := json.Marshal(req)
	httpreq, err := http.NewRequestWithContext(ctx, "POST", u, bytes.NewBuffer(b))
	if err != nil {
		return fmt.Errorf("http.NewRequest error: %w", err)
	}

	httpres, err := httpcli.Do(httpreq)
	if err != nil {
		return fmt.Errorf("httpcli.Post error: %w", err)
	}
	defer httpres.Body.Close()

	if httpres.StatusCode != 200 {
		respB, _ := ioutil.ReadAll(httpres.Body)
		// err = fmt.Errorf("'%s' returns StatusCode '%v', request '%s', response '%s'", ep.host, httpres.Status, b, respB)
		err = fmt.Errorf("'%s' returns StatusCode '%v', response '%s'", ep.host, httpres.Status, respB)
		return
	}

	b, err = ioutil.ReadAll(httpres.Body)
	if err != nil {
		err = fmt.Errorf("ioutil.ReadAll error: %w", err)
		return
	}

	err = json.Unmarshal(b, rsp)
	if err != nil {
		return fmt.Errorf("json.Unmarshal error: %w", err)
	}

	return nil
}
