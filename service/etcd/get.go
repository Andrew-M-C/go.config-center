package etcd

import "context"

// GetRequest 模拟 clientv3.etcdserverpbRangeRequest
type GetRequest struct {
	Key      string `json:"key"`                 // base64
	RangeEnd string `json:"range_end,omitempty"` // base64
}

// GetResponse 模拟 clientv3.etcdserverpbRangeResponse
type GetResponse struct {
	Header ResponseHeader `json:"header"`
	Mode   bool           `json:"more"`
	Kvs    []*Kv          `json:"kvs"`
}

// Kv 模拟 clientv3.mvccpbKeyValue
type Kv struct {
	Key            string `json:"key"`
	Value          []byte `json:"-"`
	ValueInString  string `json:"value"`
	Lease          string `json:"lease"`
	Version        string `json:"version"`
	CreateRevision string `json:"create_revision"`
	ModRevision    string `json:"mod_revision"`
}

// Get 模拟 clientv3.Get 操作
func (cli *Client) Get(ctx context.Context, k string, opts ...*OpOption) (rsp *GetResponse, err error) {
	resp := GetResponse{}
	req := GetRequest{Key: b64Enc(k)}

	if len(opts) > 0 && opts[0] != nil {
		op := opts[0]
		if op.rangeEnd != "" {
			req.RangeEnd = b64Enc(op.rangeEnd)
		}
	}

	err = cli.post(ctx, "kv/range", &req, &resp)
	if err != nil {
		return
	}

	for _, kv := range resp.Kvs {
		kv.Key = b64Dec(kv.Key)
		kv.Value = b64DecB(kv.ValueInString)
	}
	return &resp, nil
}
