package etcdkv

import (
	mysqlkv "gitlab.com/Andrew-M-C/go.mysqlkv"

	"gitlab.com/Andrew-M-C/go.config-center/service/etcd"
)

// NewStorage 返回一个 KV 存储工具
func NewStorage(conf etcd.Config) (mysqlkv.Storage, error) {
	return newImpl(&conf)
}
