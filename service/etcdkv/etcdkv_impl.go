package etcdkv

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"

	jsonvalue "github.com/Andrew-M-C/go.jsonvalue"
	swg "github.com/remeh/sizedwaitgroup"
	mysqlkv "gitlab.com/Andrew-M-C/go.mysqlkv"

	"gitlab.com/Andrew-M-C/go.config-center/service/etcd"
)

// type Storage interface {
// 	SetByStruct(businessCode int32, object string, data interface{}) error
// 	SetByJSONValue(businessCode int32, object string, data *jsonvalue.V) error
// 	GetToJSONValue(businessCode int32, object string, keys []string) (*jsonvalue.V, error)
// }

// etcdKVImpl
type etcdKVImpl struct {
	cli *etcd.Client
}

// newImpl 返回 kv 的实际实现
func newImpl(conf *etcd.Config) (impl *etcdKVImpl, err error) {
	cli, err := etcd.New(*conf)
	if err != nil {
		err = fmt.Errorf("connect etcd error: %w", err)
		return nil, err
	}

	return &etcdKVImpl{
		cli: cli,
	}, nil
}

// ======== SetByStruct ========

// SetByJSONValue SetByJSONValue
func (impl *etcdKVImpl) SetByJSONValue(busiCode int32, object string, v *jsonvalue.V) (err error) {
	if !v.IsObject() {
		return errors.New("data is not an object")
	}
	if "" == object {
		return mysqlkv.EmptyKey
	}

	// 搜罗出所有的 KV 对
	keys, kvMap, err := validateAndPackKeysFromJSONValue(v, "", nil, nil)
	if err != nil {
		return err
	}

	busiCodeStr := fmt.Sprintf("%06d.", busiCode)
	genKey := func(k string) string {
		return busiCodeStr + object + "." + k
	}

	keysToDelete := []string{}
	ops := []etcd.Op{}
	for _, k := range keys {
		v := kvMap[k]
		if v.IsObject() {
			ops = append(ops, etcd.OpPut(genKey(k), "{}"))
		} else {
			ops = append(ops, etcd.OpPut(genKey(k), v.MustMarshalString()))
			if v.IsNull() {
				keysToDelete = append(keysToDelete, genKey(k))
			}
		}
	}

	tx := impl.cli.Txn(context.TODO())
	if tx == nil {
		return errors.New("transaction nil")
	}

	resp, err := tx.Then(ops...).Commit()
	if err != nil {
		err = fmt.Errorf("transaction error: %w", err)
		return
	}

	if !resp.Succeeded {
		err = errors.New("update config failed")
		return
	}

	// 删除 keys
	// 这些暂时没想好怎么原子化
	return impl.deleteKeys(keysToDelete)
}

func (impl *etcdKVImpl) deleteKeys(keys []string) (err error) {
	if len(keys) == 0 {
		return nil
	}
	// log.Printf("request delete keys: %+v", keys)

	wg := swg.New(10)
	lock := sync.Mutex{}

	setErr := func(e error) {
		lock.Lock()
		err = e
		lock.Unlock()
	}

	for _, k := range keys {
		if k == "" {
			continue
		}

		wg.Add()
		go func(k string) {
			defer wg.Done()

			le := len(k)
			end := fmt.Sprintf("%s%c", k[:le-1], k[le-1]+1)

			resp, e := impl.cli.Get(context.TODO(), k, etcd.WithRange(end))
			if e != nil {
				e = fmt.Errorf("cli.Get error: %w", e)
				setErr(e)
				return
			}

			for _, kv := range resp.Kvs {
				// log.Printf("Got key: %s", string(kv.Key))
				_, e = impl.cli.Delete(context.TODO(), string(kv.Key))
				if e != nil {
					e = fmt.Errorf("Delete %s error: %w", string(kv.Key), err)
					setErr(e)
					return
				}
			}

			return
		}(k)
	}

	wg.Wait()
	return err
}

func validateAndPackKeysFromJSONValue(
	v *jsonvalue.V, prefix string, l []string, m map[string]*jsonvalue.V,
) ([]string, map[string]*jsonvalue.V, error) {
	if nil == l {
		l = []string{}
	}
	if nil == m {
		m = map[string]*jsonvalue.V{}
	}

	var err error

	for it := range v.IterObjects() {
		k := it.K
		v := it.V

		if err = validateKey(k); err != nil {
			return nil, nil, err
		}

		fullKey := prefix + k
		l = append(l, fullKey)
		m[fullKey] = v

		if v.IsObject() {
			l, m, err = validateAndPackKeysFromJSONValue(v, fullKey+".", l, m)
			if err != nil {
				return nil, nil, err
			}
		}
	}

	return l, m, nil
}

func validateKey(key string) error {

	if l := len(key); l == 0 {
		return mysqlkv.EmptyKey
	} else if l >= mysqlkv.MaxKeyLen {
		return mysqlkv.KeyIsTooLong
	}

	for _, r := range key {
		if r >= 'a' && r <= 'z' {
			continue
		}
		if r >= 'A' && r <= 'Z' {
			continue
		}
		if r >= '0' && r <= '9' {
			continue
		}
		if r == '-' {
			continue
		}
		if r == '_' {
			continue
		}
		return fmt.Errorf("%w: %v", mysqlkv.IllegalKeyCharacter, r)
	}

	return nil
}

// ======== SetByStruct ========

// SetByStruct SetByStruct
func (impl *etcdKVImpl) SetByStruct(businessCode int32, object string, data interface{}) (err error) {
	v, err := validateSetByStructReq(object, data)
	if err != nil {
		return
	}

	return impl.SetByJSONValue(businessCode, object, v)
}

func validateSetByStructReq(object string, data interface{}) (*jsonvalue.V, error) {
	b, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal error: %w", err)
	}
	if object == "" {
		return nil, errors.New("missing object ID")
	}

	v, err := jsonvalue.Unmarshal(b)
	if err != nil {
		return nil, fmt.Errorf("json.Unmarshal error: %w", err)
	}
	if !v.IsObject() {
		return nil, errors.New("data is not an object")
	}

	return v, nil
}

// ======== GetToJSONValue ========

// GetToJSONValue GetToJSONValue
func (impl *etcdKVImpl) GetToJSONValue(busiCode int32, object string, keys []string) (ret *jsonvalue.V, err error) {
	keyRangeList := make([][]string, 0, len(keys))
	if object == "" {
		if len(keys) > 0 {
			return nil, mysqlkv.EmptyKey
		}
		begin := fmt.Sprintf("%06d", busiCode)
		end := fmt.Sprintf("%06d", busiCode+1)
		keyRangeList = [][]string{{begin, end}}

	} else if len(keys) == 0 {
		le := len(object)
		begin := fmt.Sprintf("%06d.%s", busiCode, object)
		end := fmt.Sprintf("%06d.%s%c", busiCode, object[:le-1], object[le-1]+1)
		keyRangeList = [][]string{{begin, end}}

	} else {
		for _, k := range keys {
			for _, part := range strings.Split(k, ".") {
				if err = validateKey(part); err != nil {
					return
				}
			}

			le := len(k)
			begin := fmt.Sprintf("%06d.%s.%s", busiCode, object, k)
			end := fmt.Sprintf("%06d.%s.%s%c", busiCode, object, k[:le-1], k[le-1]+1)
			keyRangeList = append(keyRangeList, []string{begin, end})

			// log.Printf("request FROM %s => %s", begin, end)
		}
	}

	ret = jsonvalue.NewObject()
	wg := swg.New(10)
	lock := sync.Mutex{}

	setErr := func(e error) {
		lock.Lock()
		err = e
		lock.Unlock()
	}

	appendResp := func(resp *etcd.GetResponse) {
		kvList := []*kvCache{}
		for _, kv := range resp.Kvs {
			// log.Printf("Got: %s - %s", kv.Key, kv.Value)

			k := string(kv.Key)
			keys := strings.Split(k, ".")
			if len(keys) <= 2 {
				continue
			}
			v, e := jsonvalue.Unmarshal(kv.Value)
			if e != nil {
				// log.Printf("unmarshal %s error: %v", kv.Value, e)
				continue
			}

			intf := make([]interface{}, 0, len(keys))
			for i := 2; i < len(keys); i++ {
				intf = append(intf, keys[i])
			}
			kvList = append(kvList, &kvCache{
				keys:    keys[2:],
				keyIntf: intf,
				v:       v,
			})
		}

		lock.Lock()
		for _, kv := range kvList {
			// log.Printf("Got: %v - %v", kv.keys, kv.v)
			ret.Set(kv.v).At(kv.keyIntf[0], kv.keyIntf[1:]...)
		}
		lock.Unlock()
	}

	for _, kPair := range keyRangeList {
		wg.Add()
		start := kPair[0]
		end := kPair[1]

		go func(start, end string) {
			defer wg.Done()
			resp, e := impl.cli.Get(context.TODO(), start, etcd.WithRange(end))
			if e != nil {
				setErr(fmt.Errorf("cli.Get error: %w", e))
				return
			}

			appendResp(resp)
		}(start, end)
	}

	wg.Wait()
	return ret, nil
}

type kvCache struct {
	keys    []string
	keyIntf []interface{}
	v       *jsonvalue.V
}
