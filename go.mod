module gitlab.com/Andrew-M-C/go.config-center

go 1.15

require (
	github.com/Andrew-M-C/go.jsonvalue v1.0.4-0.20210125112232-180cec9f8c2d
	github.com/Andrew-M-C/go.mysqlx v0.3.1-0.20210120085908-c415175f4d91
	github.com/Andrew-M-C/go.util v0.0.0-20210903031003-4865769a9fb8
	github.com/gin-gonic/gin v1.6.3
	github.com/kr/pretty v0.2.1 // indirect
	github.com/remeh/sizedwaitgroup v1.0.0
	github.com/sirupsen/logrus v1.7.0
	gitlab.com/Andrew-M-C/go.httpsess v0.0.0-20210308141247-3eb861a4dc85
	gitlab.com/Andrew-M-C/go.logger v0.0.0-20201117120927-faffb61ece9b
	gitlab.com/Andrew-M-C/go.mysqlkv v0.0.0-20210204020846-5f8b847c111c
)
