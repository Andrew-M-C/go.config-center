#
FROM alpine:3.12.0
LABEL maintainer="Andrew-M-C"
COPY config-center /srv/
EXPOSE 80
ENTRYPOINT ["/srv/config-center"]