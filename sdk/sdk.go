package sdk

import (
	"os"
	"strings"

	jsonvalue "github.com/Andrew-M-C/go.jsonvalue"
)

// SDK 表示配置中心 SDK
type SDK interface {
	GetToJSONValue(domain string) (*jsonvalue.V, error)
}

// Host 将 127.0.0.1 和 localhost 等环回地址解析为在环境中可用的地址
func Host(host string) string {
	if loopAlternate == "" {
		return host
	}

	if strings.HasPrefix(host, "localhost") {
		return strings.Replace(host, "localhost", loopAlternate, 1)
	}
	if strings.HasPrefix(host, "127.0.0.1") {
		return strings.Replace(host, "127.0.0.1", loopAlternate, 1)
	}
	if strings.HasPrefix(host, "::1") {
		return strings.Replace(host, "::1", loopAlternate, 1)
	}

	return host
}

// ======== 判断当前工作环境 ========
// reference:
// - [如何判断我的容器是否在Kubernetes集群中运行？](https://www.thinbug.com/q/36639062)
// - [K8s](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/#environment-variables)

var (
	loopAlternate = ""
)

func init() {
	determineEnv()
}

func determineEnv() {
	// In K8s?
	if h := os.Getenv("KUBERNETES_SERVICE_HOST"); h != "" {
		loopAlternate = "kubernetes.docker.internal"
		return
	}

	// In docker?
	_, err := os.Stat("/.dockerenv")
	if err == nil {
		loopAlternate = "host.docker.internal"
		return
	}

	// unknown
	return
}
