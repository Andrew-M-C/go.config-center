package http

import (
	"gitlab.com/Andrew-M-C/go.config-center/sdk"
)

// NewHTTP 返回基于 HTTP 接口的 SDK
func NewHTTP(host string) (sdk.SDK, error) {
	return newSDKImpl("http", host)
}

// NewHTTPS 返回基于 HTTP 接口的 SDK
func NewHTTPS(host string) (sdk.SDK, error) {
	return newSDKImpl("https", host)
}
