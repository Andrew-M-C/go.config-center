package http

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	jsonvalue "github.com/Andrew-M-C/go.jsonvalue"
)

type sdkImpl struct {
	protocol string
	host     string
	getURL   string

	transport http.RoundTripper
}

func newSDKImpl(protocol, host string) (*sdkImpl, error) {
	impl := &sdkImpl{
		protocol:  protocol,
		host:      host,
		getURL:    fmt.Sprintf("%s://%s/v1/config/common/query", protocol, host),
		transport: http.DefaultTransport,
	}

	_, err := url.Parse(impl.getURL)
	if err != nil {
		return nil, fmt.Errorf("invalid host %s, error: %w", host, err)
	}
	return impl, nil
}

func (impl *sdkImpl) GetToJSONValue(domain string) (res *jsonvalue.V, err error) {
	cli := &http.Client{
		Transport: impl.transport,
		Timeout:   20 * time.Second,
	}

	reqV := jsonvalue.NewObject(map[string]interface{}{
		"domain": domain,
	})

	request, _ := http.NewRequest("POST", impl.getURL, bytes.NewBuffer(reqV.MustMarshal()))
	request.Header.Set("Content-type", "application/json;charset=utf-8")

	response, err := cli.Do(request)
	if err != nil {
		err = fmt.Errorf("client.Do error: %w", err)
		return
	}

	defer response.Body.Close()

	if response.StatusCode != 200 {
		err = fmt.Errorf("HTTP StatusCode: %v", response.StatusCode)
		return
	}

	respB, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("ioutil.ReadAll error: %w", err)
	}

	respV, err := jsonvalue.Unmarshal(respB)
	if err != nil {
		return nil, fmt.Errorf("Unmarshal error: %w", err)
	}

	code, err := respV.GetInt("code")
	if err != nil {
		return nil, fmt.Errorf("read response code error: %w", err)
	}

	if code != 0 {
		msg, _ := respV.GetString("message")
		return nil, fmt.Errorf("%d-%s", code, msg)
	}

	domainParts := strings.Split(domain, ".")
	data, _ := respV.Get("data", domainParts[len(domainParts)-1])
	if data == nil {
		data = jsonvalue.NewNull()
	}

	return data, nil
}
