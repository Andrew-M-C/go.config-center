package http

import "testing"

func TestHTTP(t *testing.T) {
	_, err := NewHTTP("123blahbl ah")
	if err == nil {
		t.Error("could not recognize illegal host")
		return
	}
	t.Logf("Got expected error: %v", err)

	s, err := NewHTTP("localhost:8002")
	if err != nil {
		t.Errorf("NewHTTP error: %w", err)
		return
	}

	conf, err := s.GetToJSONValue("global.mysql")
	if err != nil {
		t.Errorf("GetToJSONValue error: %w", err)
		return
	}
	t.Logf("Got response: %v", conf.MustMarshalString())

	conf, err = s.GetToJSONValue("global.discovery.endpoints")
	if err != nil {
		t.Errorf("GetToJSONValue error: %w", err)
		return
	}

	t.Logf("Got response: %v", conf.MustMarshalString())
	return
}
