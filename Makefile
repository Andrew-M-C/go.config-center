
TARGET_NAME = config-center

# 这是为了解决私有库的问题：[Go填坑之将Private仓库用作module依赖](https://segmentfault.com/a/1190000021127791)
# 原文：
# 在1.13版本之后，前面介绍的解决方案又会导致go get出现另一种错误：
# get "gitlab.com/xxx/zz": found meta tag get.metaImport{Prefix:"gitlab.com/xxx/zz", VCS:"git", RepoRoot:"https://gitlab.com/xxx/zz.git"} at //gitlab.com/xxx/zz?go-get=1
#   verifying gitlab.com/xxx/zz@v0.0.1: gitlab.com/xxx/zz@v0.0.1: reading https://sum.golang.org/lookup/gitlab.com/xxx/zz@v0.0.1: 410 Gone
# 这个错误是因为新版本go mod会对依赖包进行checksum校验，但是私有仓库对sum.golang.org是不可见的，它当然没有办法成功执行checksum。
# 也就是说强制git采用ssh的解决办法在1.13版本之后GG了。
# 当然Golang在堵上窗户之前，也开了大门，它提供了一个更方便的解决方案：GOPRIVATE环境变量。解决以上的错误，可以这样配置：
# export GOPRIVATE=gitlab.com/xxx
# 它可以声明指定域名为私有仓库，go get在处理该域名下的所有依赖时，会直接跳过GOPROXY和CHECKSUM等逻辑，从而规避掉前文遇到的所有问题。
# 
# 此外，关闭 GOSUMDB 也是可以解决这个问题的
export GOPRIVATE=gitlab.com/Andrew-M-C
# export GOSUMDB=off

export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64

GIT_HASH = $(shell git log -1 --abbrev-commit | grep commit | cut -f 2 -d ' ')
IS_CLEAN = _$(shell git status . | grep -o "clean" | sed 's/ /_/g')
NOW = $(shell date +%Y%m%d)

.PHONY: all
all: $(TARGET_NAME)

.PHONY: $(TARGET_NAME)
$(TARGET_NAME):
	go build -o $(TARGET_NAME) .
	@if [ $(IS_CLEAN) = "_" ]; then \
		echo $(TARGET_NAME) $(GIT_HASH) \(modified\) built.; \
	else \
		echo $(TARGET_NAME) $(GIT_HASH) built.;\
	fi
	

.PHONY: clean
clean:
	@go clean
	-@rm -f $(TARGET_NAME) 2>/dev/null
	@echo $(TARGET_NAME) cleaned.

.PHONY: image
image: all
	@if [ $(IS_CLEAN) = "_" ]; then \
		docker image build -t $(TARGET_NAME):$(GIT_HASH)_modified_$(NOW) .; \
	else \
		docker image build -t $(TARGET_NAME):$(GIT_HASH) .; \
	fi

# docker container run -d --name linkage-server -e DB_HOST=host.docker.internal -p 8000:80 linkage_server:1eb24fb_modified_20201017
