package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	httpconfig "gitlab.com/Andrew-M-C/go.config-center/server/http/config"
	"gitlab.com/Andrew-M-C/go.config-center/service/etcd"
	"gitlab.com/Andrew-M-C/go.config-center/service/etcdkv"

	mysqlx "github.com/Andrew-M-C/go.mysqlx"
	timeutil "github.com/Andrew-M-C/go.util/time"
	gin "github.com/gin-gonic/gin"
	ginsess "gitlab.com/Andrew-M-C/go.httpsess/gin"
	logger "gitlab.com/Andrew-M-C/go.logger"
	mysqlkv "gitlab.com/Andrew-M-C/go.mysqlkv"
)

var (
	configServer httpconfig.Endpoint
	logToFile    bool
)

func initEnv(l logger.L) (err error) {
	// 获取 etcd 配置
	etcdEndpointsStr := os.Getenv("ETCD_ENDPOINTS")
	if etcdEndpointsStr != "" {
		return initEnvByEtcd(l, etcdEndpointsStr)
	}

	return initEnvByMySQL(l)
}

func initEnvByEtcd(l logger.L, epStr string) (err error) {
	// ETCD_ENDPOINTS=localhost:21379,localhost:22379,localhost:23379 go run .
	l.Infof("request etcd endpoints: %s", epStr)

	endpoints := strings.Split(epStr, ",")
	conf := etcd.Config{
		Endpoints:   endpoints,
		DialTimeout: 2 * time.Second,
	}

	kv, err := etcdkv.NewStorage(conf)
	if err != nil {
		err = fmt.Errorf("etcdkv.NewStorage error: %w", err)
		l.Error(err)
		return
	}

	configServer, err = httpconfig.NewEndpoint(kv)
	if err != nil {
		err = fmt.Errorf("httpconfig.NewEndpoint error: %w", err)
		l.Error(err)
		return
	}

	return nil
}

func initEnvByMySQL(l logger.L) (err error) {
	// 获取 mysql 配置
	dbHost := os.Getenv("DB_HOST")
	if dbHost == "" {
		dbHost = "127.0.0.1"
	}

	l.Infof("request database host: %v", dbHost)
	mysql := mysqlx.Param{
		Host:   dbHost,
		Port:   3306,
		User:   "root",
		Pass:   "123456",
		DBName: "db_test",
	}

	// 获取 mysqlkv 实例
	kv, err := mysqlkv.NewStorage(mysql, true)
	if err != nil {
		err = fmt.Errorf("mysqlkv.NewStorage error: %w", err)
		l.Error(err)
		return
	}

	configServer, err = httpconfig.NewEndpoint(kv)
	if err != nil {
		err = fmt.Errorf("httpconfig.NewEndpoint error: %w", err)
		l.Error(err)
		return
	}

	return nil
}

func main() {
	l := logger.NewStdout()

	l.Infof("Entered server")
	defer l.Infof("Server exit")

	timeutil.SleepToNextSecondsN(6)

	err := initEnv(l)
	if err != nil {
		time.Sleep(10 * time.Microsecond)
		return
	}

	// 初始化 gin
	router := gin.New()
	router.Use(func(c *gin.Context) {
		start := time.Now()
		c.Next()
		l.Infof(
			"%s %s DONE, elapsed %v, return STATUS %d, client IP '%v'",
			c.Request.Method,
			c.Request.RequestURI,
			time.Since(start),
			c.Writer.Status(),
			c.ClientIP(),
		)
	})
	router.Use(gin.Recovery())

	// 初始化路由
	router.POST("/", func(c *gin.Context) {
		c.String(200, "Hello world!")
	})

	router.POST("/v1/config/common/query", func(c *gin.Context) {
		l := getLogger("/srv/config_queryConfig.log")
		sess := ginsess.NewGinSession(c, l)
		configServer.QueryConfig(sess)
	})

	router.POST("/v1/config/common/update", func(c *gin.Context) {
		l := getLogger("/srv/config_updateConfig.log")
		sess := ginsess.NewGinSession(c, l)
		configServer.UpdateConfig(sess)
	})

	// 判断是否需要将日志打到文件中
	logToFileEnv := os.Getenv("LOG_FILE")
	l.Infof("LOG_FILE='%s'", logToFileEnv)
	if logToFileEnv == "0" || logToFileEnv == "" {
		logToFile = false
	} else {
		logToFile = true
	}

	// 获取端口并启动服务
	portEnv := os.Getenv("HTTP_PORT")
	if portEnv == "" {
		portEnv = "8002"
	}

	done := make(chan struct{}, 10)

	go func(portEnv string) {
		l.Infof("Now http.ListenAndServe() at: %v", portEnv)
		err := router.Run(":" + portEnv)
		if err != nil {
			l.Errorf("router.Run error: %v", err)
		}
		l.Infof("HTTP exit")
		done <- struct{}{}
	}(portEnv)

	<-done
	l.Infof("process exit")
}

func getLogger(filepath string) logger.L {
	if !logToFile {
		return logger.NewStdout()
	}
	return logger.NewHourLogger(filepath)
}
